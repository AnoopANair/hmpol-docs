# Implemetation details

HM-Pol code is implemented as a wrapper for the following backend calculator software:

![Wrapper implementation Logo](WI.png)

The code can be installed and used for the respective backend software as follows:

![pyscf Logo](WI-Pyscf.png)

Installation:

    !conda create --name HMPOL
    !conda activate HMPOL
    !pip3 install hmpol

Usage:


