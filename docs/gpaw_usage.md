## Usage with GPAW


The H-MPol implementation in GPAW, can be used from the theochem-UI modification of GPAW.

### Installation:

**Dependencies** 

Create an environment: 
```bash
conda create --name HMPOL
conda activate HMPOL
```
Install dependencies:
```bash
conda install -c conda-forge ase
conda install cmake
conda install -c conda-forge gcc
conda install -c conda-forge libxc
conda install blas blas-devel libblas libcblas liblapack liblapacke
conda install scalapack
conda install openmpi openmpi-mpicc openmpi-mpicxx openmpi-mpifort
conda install conda-forge::fftw
```
**Download and Install GPAW**

```bash
git clone https://gitlab.com/AnoopANair/gpaw_hmpol.git
```
```bash
cd gpaw_hmpol
python setup.py build_ext
python setup.py install
```


Usage: Let's use water (**$H_2O.xyz$** as given below) as an example:
```
3
Properties=species:S:1:pos:R:3 pbc="F F F"
O       -0.00000000       0.00000000       0.06664447
H        0.76804750       0.00000000      -0.52889132
H       -0.76804750       0.00000000      -0.52889132
```
We will calculate the dipole-quadrupole and quadrupole-quadrupole polarizability tensors.

```python
from ase.io import read,  write 
from gpaw import GPAW
from gpaw.eigensolvers import RMMDIIS
from ase.io import read
import numpy as np
from ase.units import Ha, Bohr
from ase.optimize import BFGS
from sim import calculate_polval

gpaw_args = dict(convergence={'qpoles': 1e-06,
                              'dpoles': 1e-06,
                              'eigenstates': 1e-08,
                              'density': 1e-08},
                 h=0.18, xc='PBE',
                 eigensolver=RMMDIIS(niter=5))                                                   

# Read file via ASE

file = "H2O.xyz"
molecule = read(file)
molecule.positions = molecule.get_positions()*Bohr
molecule.center(vacuum=7.0)


calc = GPAW(**gpaw_args)
molecule.set_calculator(calc)

# Do a BFGS geometry optimization
dyn = BFGS(molecule, trajectory = '{}PBE.traj'.format(file[:-4]))
dyn.run(fmax = 0.005)

# Calculate the polarizability tensors of the optimized structure : via the extrapolation scheme.
PathSource = '{}PBE.traj'.format(file[:-4])

for i in [1,0.1,0.01]:
    FF =  0.0486/Ha*Bohr*Bohr
    F =  0.486/Ha*Bohr
    calculate_polval(PathSource, F*i, FF*i, 'BLYP')


```