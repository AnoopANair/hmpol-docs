## Usage with PySCF


The pip installable package is associated with **PySCF**.
Installation:
```bash
!conda create --name HMPOL
!conda activate HMPOL
!pip3 install hmpol
```
Usage: Let's use water (**$H_2O.xyz$** as given below) as an example:
```
3
Properties=species:S:1:pos:R:3 pbc="F F F"
O       -0.00000000       0.00000000       0.06664447
H        0.76804750       0.00000000      -0.52889132
H       -0.76804750       0.00000000      -0.52889132
```
We will calculate the dipole-quadrupole and quadrupole-quadrupole polarizability tensors.

```python
import sys
from pyscf import gto
from ase.io import read
from poltensor import Pols
import numpy as np 

mol = gto.Mole()
molecule = read("H2O.xyz")
mol.atom = "H2O.xyz" 
mol.basis = "augccpvqz"
mol.build()

AIJKIrrep   = np.load("H2O_mono_AIJK_Trrep.npy")
AIJK        = np.load("H2O_mono_AIJK.npy")
CIJKLIrrep  = np.load("H2O_mono_CIJKL_Trrep.npy")
CIJKL       = np.load("H2O_mono_CIJKL.npy")
```